<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="include/style/formcontrol.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Include the above in your HEAD tag ---------->
</head>
<body>
  <div class="container contact-form">
    <div class="contact-image">
      <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
    </div>
    <form method="get" action="nfe_be.php">
      <h3>Adicionar informações da nota fiscal</h3>
      <div class="row">
        <div class="col-md-12">
          <?php
          ini_set("error_reporting",E_ALL);
          ini_set('display_errors', 1);
          require 'include/all_include.php';
          echo "<pre>";
          $b2w_order = new order;
          $flux = new flux('b2w_order');
          $flux->nfeFile = true;
          $flux->list_item = json_decode(file_get_contents('include/files/b2w_order_nfe'));
          $flux->pathFiles = 'include/files/b2w_order_last_nfe';
          $flux->setFiles();
          $flux->getFiles();
          $order_id = $flux->next_item();

          if(!$order_id) {
            echo '<h2>NÃO HÁ NOVOS PEDIDOS</h2>';
            exit;
          }
          // get informations about order id and decodes it
          $order_data = json_decode($b2w_order->get_order_information($order_id)->response);

          // echo "<pre>";             //DEBUG
          // var_dump($order_id);  //DEBUG
          // if the variable erro is set on url, do echo with mensage "DO NOT POSSIBLE ADD NFE KEY ON THIS ORDER ID"


          if(isset($_GET['erro'])) echo '<h6 style="color:red;">Não foi possível adicionar a chave ao pedido '.$order_id.'</h6>';

          if(isset($_GET['sucesso'])) echo '<h6 style="color:green;">Adicionado chave nfe ao pedido '.$order_id.' com sucesso</h6>';

          // if($_GET['sem_pedido']) echo '<h2>NÃO HÁ PEDIDOS SEM A CHAVE NF2</h2>';
          // if the variable erro isn't set, verify if invoices property is empty and type property is iguals of APPROVED
          if((empty($order_data->invoices)) && ($order_data->status->type == "APPROVED")) {
            $id_pedido = $order_data->code;
            ?>
            <div class="form-group">
              <h2>Código do Pedido</h2>
              <input type="text" name="id_pedido" class="form-control" hidden='true' readyonly="true" value="<?php echo $id_pedido; ?>"/>
              <label type="text" name="id_pedido" class="form-control" readyonly="true" value=""><?php echo $id_pedido; ?></label><br>
              <h2>Produto(S)</h2>
              <?php
              foreach ($order_data->items as $key => $value) {
                echo '<label type="text" name="produto" class="form-control" readyonly="true" value="">'.$order_data->items[$key]->name.'</label><br>';
              }?>
            </div>
            <label type="text" name="customer_name" class="form-control" readyonly="true" value=""><?php echo "Cliente: ".$order_data->customer->name; ?></label><br>
            <label type="text" name="receiver_name" class="form-control" readyonly="true" value=""><?php echo "Receptor: ".$order_data->shipping_address->full_name; ?></label><br>
            <h2>Produto(S)</h2>
            <?php
            foreach ($order_data->items as $key => $value) {
              echo '<label type="text" name="produto" class="form-control" readyonly="true" value="">'.$order_data->items[$key]->name.'</label><br>';
            }?>
            <div class="radio" class="col-md-4">
              <h2>NFE</h2>
              <textarea name="nfe" value="" cols="75" rows="1"></textarea><br><br>
            </div>
            <div class="form-group">
              <input type="submit" name="btnSubmit" class="btnContact" value="Adicionar"/>
            </div>
            <?php
          } else {
            // if invoices property isn't empty and type property is different of APPROVED, do echo with the html and message Have not orders without NFE key
            echo '<h2>NÃO HÁ PEDIDOS SEM A CHAVE NF2</h2>';
            $flux->add_item($order_id);
          }

          ?>
        </div>
      </div>
    </form>
  </div>
</body>

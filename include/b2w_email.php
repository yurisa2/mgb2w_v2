<?php
class email
{
  public function __construct($orderData)
  {
    $this->orderData = $orderData;
  }

  private function message()
  {
    $product = '';
    foreach ($this->orderData->items as $key => $value) {
      $product .= "Produto: ".$value->name."<br>Sku: ".$value->id."<br>Quantidade: ".$value->qty.
      "<br>Preço Especial do Produto: ".$value->special_price."<br>Preço Original do Produto: ".$value->original_price;
    }
    $payment = 0;
    $parcels = 0;
    foreach ($this->orderData->payments as $key => $value)
    {
      $payment += (float)$value->value;
      $parcels += (float)$value->parcels;
      $paymentMethod = $value->method;
    }
    $message_email = "Nome do Comprador: ".$this->orderData->customer->name
    ."<br>Email do Comprador: ".$this->orderData->customer->email
    ."<br>Telefone do Comprador: ".$this->orderData->shipping_address->phone
    ."<br>Documento do comprador: ".$this->orderData->customer->vat_number
    ."<br>".$product
    ."<br>Custo de Envio: ".$this->orderData->shipping_cost
    ."<br>Pedido: ".$this->orderData->code
    ."<br>Pagamento: ".$paymentMethod
    ."<br>Parcelas: ".$parcels
    ."<br>Desconto: ".$this->orderData->discount
    ."<br>Total Pago: ".$payment
    ."<br>Receptor da Entrega: ".$this->orderData->shipping_address->full_name
    ."<br>Rua da Entrega: ".$this->orderData->shipping_address->street
    ."<br>Número da Entrega: ".$this->orderData->shipping_address->number
    ."<br>Bairro da Entrega: ".$this->orderData->shipping_address->neighborhood
    ."<br>Cep da Entrega: ".$this->orderData->shipping_address->postcode
    ."<br>Cidade da Entrega: ".$this->orderData->shipping_address->city
    ."<br>Estado da Entrega: ".$this->orderData->shipping_address->region
    ."<br>País da Entrega: ".$this->orderData->shipping_address->country
    ."<br>Telefone de Contato: ".$this->orderData->shipping_address->phone
    ."<br>Referência do Local de Entrega: ".$this->orderData->shipping_address->reference
    ."<br>Receptor da Cobrança : ".$this->orderData->billing_address->full_name

    ."<br>Rua da Cobrança : ".$this->orderData->billing_address->street
    ."<br>Número da Cobrança : ".$this->orderData->billing_address->number
    ."<br>Bairro da Cobrança : ".$this->orderData->billing_address->neighborhood
    ."<br>Cep da Cobrança : ".$this->orderData->billing_address->postcode
    ."<br>Cidade da Cobrança : ".$this->orderData->billing_address->city
    ."<br>Estado da Cobrança : ".$this->orderData->billing_address->region
    ."<br>País da Cobrança : ".$this->orderData->billing_address->country
    ."<br>Telefone de Contato: ".$this->orderData->billing_address->phone
    ."<br>Referência do Local da Cobrança: ".$this->orderData->billing_address->reference
    ."<br><br><br>Acesse: ". NFE_URL." para adicionar a chave da Nota Fiscal no pedido";

    return $message_email;
  }

  public function sendOrderLabel()
  {
    $email_message = $this->message();
    // var_dump($email_message);   //DEBUG
    echo "criação do json";
    var_dump(file_put_contents(str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.str_replace(' ','',$this->orderData->code.'.json'),$email_message));
  }
}
?>

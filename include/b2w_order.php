<?php
class b2w_order extends order
{
  public function __construct()
  {
    parent::__construct();
    $this->wooCommerceOrder = new wooCommerceOrder;
    $this->wooCommerceCustomer = new wooCommerceCustomer;
  }

  public function orderCreate($next_order_id)
  {

    // PEGA OS DADOS DO PEDIDO
    $orderData = json_decode($this->get_order_information($next_order_id)->response);

    $normalizeCustomerData = $this->normalizeCustomerData($orderData);
    $customerList = $this->wooCommerceCustomer->WooCommerceGetCustomerList(['email'=>$normalizeCustomerData->email]);

    if(empty($customerList)) $orderData->wcCustomerId = $this->wooCommerceCustomer->wooCommerceCreateCustomer($normalizeCustomerData)->id;
    else $orderData->wcCustomerId = $customerList[0]->id;

    $normalizeOrderData = $this->normalizeOrderData($orderData);

    try {
      $createOrder = $this->wooCommerceOrder->wooCommerceCreateOrder($normalizeOrderData);
    } catch(Exception $exception) {
      $error = new error_handling("WCB2W: Erro ao criar pedido no WooCommerce", (string)$exception->getMessage(), "Pedido: $orderData->code", "Erro pedido");
      $error->send_error_email();
      $error->execute();
      echo '<br>'.$exception->getMessage();
      return $exception->getMessage();
    }

    $log = new log("Novo Pedido SKYHUB", "Id Pedido: ".$orderData->code, "Novo pedido criado no WooCommerce", "nova compra");
    $log->log_email = true;
    $log->mensagem_email = "Novo Pedido SKYHUB";
    $log->log_email = true;
    $log->email_novacompra = true;
    $log->dir_file = "log/log.json";
    $log->log_files = true;
    $log->send_log_email();
    $log->execute();
    echo "Pedido criado: ";
    var_dump($createOrder);
    return true;
  }

  public function normalizeOrderData($orderData)
  {
    $productSpecialPrice = '';
    $productOriginalPrice = '';
    $payment = 0;
    $parcels = 0;
    $subtotal = 0;
    foreach ($orderData->items as $key => $value)
    {
      $subtotal += $value->special_price*$value->qty;
      $productSpecialPrice .= $value->special_price.' ';
      $productOriginalPrice .= $value->original_price.' ';
      $normalizeData->productItems[] = ['product_id' => $value->id, 'quantity' =>$value->qty,'total' => $subtotal];
      // $normalizeData->meta_data[] = ['key' => 'productName', 'value' => $value->name];
    }
    // $normalizeData->meta_data[] = ['key' => 'productSpecialPrice', 'value' => trim($productSpecialPrice)];
    // $normalizeData->meta_data[] = ['key' => 'productShippingCost', 'value' => $value->shipping_cost];
    // $normalizeData->meta_data[] = ['key' => 'productOriginalPrice', 'value' => trim($productOriginalPrice)];

    foreach ($orderData->payments as $key => $value)
    {
      $payment += (float)$value->value;
      $parcels += (float)$value->parcels;
      $normalizeData->meta_data[] = ['key' => 'Tipo de pagamento', 'value' => $value->method];
    }
    $normalizeData->meta_data[] = ['key' => 'totalToPay', 'value' => $payment];
    $normalizeData->meta_data[] = ['key' => 'Parcelas', 'value' => $parcels];

    $normalizeData->meta_data[] = ['key' => 'birthday', 'value' => $orderData->customer->date_of_birth];

    $normalizeData->meta_data[] = ['key' => 'shippingReference', 'value' => $orderData->shipping_address->reference];
    $normalizeData->meta_data[] = ['key' => 'billingReference', 'value' => $orderData->billing_address->reference];

    $nameLastname = "B2W ".ucwords(strtolower($orderData->customer->name));

    $normalizeData->shippingName = "B2W ".ucwords(strtolower($orderData->shipping_address->full_name));
    $normalizeData->shippingLastname = '';
    $normalizeData->shippingAddress = $orderData->shipping_address->street;
    $normalizeData->shippingAddress2 = $orderData->shipping_address->detail;
    $normalizeData->shippingCity = $orderData->shipping_address->city;
    $normalizeData->shippingState = $orderData->shipping_address->region;
    $normalizeData->shippingCep = $orderData->shipping_address->postcode;
    $normalizeData->shippingCountry = $orderData->shipping_address->country;

    $normalizeData->billingName = "B2W ".ucwords(strtolower($orderData->billing_address->full_name));
    $normalizeData->billingLastname = '';
    $normalizeData->billingAddress = $orderData->billing_address->street;
    $normalizeData->billingAddress2 = $orderData->billing_address->detail;
    $normalizeData->billingCity = $orderData->billing_address->city;
    $normalizeData->billingState = $orderData->billing_address->region;
    $normalizeData->billingCep = $orderData->billing_address->postcode;
    $normalizeData->billingCountry = $orderData->billing_address->country;
    $normalizeData->billingEmail = $orderData->customer->email;

    $normalizeData->billingPhone = $orderData->shipping_address->phone;

    $normalizeData->shippingTotal = 0;

    $normalizeData->customerId = $orderData->wcCustomerId;

    $normalizeData->meta_data[] = ['key' => '_billing_cpf', 'value' => $orderData->customer->vat_number];
    $normalizeData->meta_data[] = ['key' => '_shipping_number', 'value' => $orderData->shipping_address->number];
    $normalizeData->meta_data[] = ['key' => '_shipping_neighborhood', 'value' => $orderData->shipping_address->neighborhood];
    $normalizeData->meta_data[] = ['key' => '_billing_neighborhood', 'value' => $orderData->billing_address->neighborhood];
    $normalizeData->meta_data[] = ['key' => '_billing_number', 'value' => $orderData->billing_address->number];


    $normalizeData->meta_data[] = ['key' => 'orderId', 'value' => $orderData->code];
    $normalizeData->meta_data[] = ['key' => 'subtotal', 'value' => $subtotal];
    // $normalizeData->meta_data[] = ['key' => 'discount', 'value' => $orderData->discount];
    $normalizeData->meta_data[] = ['key' => 'shipping', 'value' => $orderData->shipping_cost];
    // $normalizeData->meta_data[] = ['key' => 'commission', 'value' => $orderData->discount];


    return $normalizeData;

  }

  public function normalizeCustomerData($orderData)
  {
    $nameLastname = "B2W ".ucwords(strtolower($orderData->customer->name));
    $name = explode(' ', $nameLastname);
    $lastname = array_splice($name, -1);
    $name = implode(' ',$name);
    $lastname = implode(' ',$lastname);

    $username = explode(' ', strtolower($orderData->customer->name));
    array_splice($username, 1);
    $username = strtolower(implode('',$username).'.'.$lastname);
    $orderCustomerData = new stdClass;
    $orderCustomerData->email = $orderData->customer->email;
    $orderCustomerData->firstName = $name;
    $orderCustomerData->lastName = $lastname;
    $orderCustomerData->username = $username;
    $orderCustomerData->billingFirstName = $name;
    $orderCustomerData->billingLastname = $lastname;
    $orderCustomerData->billingAddress = $orderData->billing_address->street.','.$orderData->billing_address->number.'-'.$orderData->billing_address->neighborhood;
    $orderCustomerData->billingAddress2 = $orderData->billing_address->detail;
    $orderCustomerData->billingCity = $orderData->billing_address->city;
    $orderCustomerData->billingState = $orderData->billing_address->region;
    $orderCustomerData->billingCep = $orderData->billing_address->postcode;
    $orderCustomerData->billingCountry = $orderData->billing_address->country;
    $orderCustomerData->billingEmail = $orderData->customer->email;
    $orderCustomerData->billingPhone = $orderData->customer->phones[0];
    $orderCustomerData->shippingFirstName = $name;
    $orderCustomerData->shippingLastname = $lastname;
    $orderCustomerData->shippingAddress = $orderData->shipping_address->street.','.$orderData->shipping_address->number.'-'.$orderData->shipping_address->neighborhood;
    $orderCustomerData->shippingAddress2 = $orderData->shipping_address->detail;
    $orderCustomerData->shippingCity = $orderData->shipping_address->city;
    $orderCustomerData->shippingState =$orderData->shipping_address->region;
    $orderCustomerData->shippingCep =  $orderData->shipping_address->postcode;
    $orderCustomerData->shippingCountry =$orderData->shipping_address->country;
    $orderCustomerData->cpf = $orderData->customer->vat_number;

    return $orderCustomerData;
  }

  public function get_last_b2wOrder($mktplaceOrderId,$limit)
  {
    try {
      $salesOrder = $this->wooCommerceOrder->wooCommerceGetOrders(['order'=>'desc','per_page'=>$limit])->orders;
      // var_dump($salesOrder);
      // exit;
      if(count($salesOrder) <= $limit) {
        foreach ($salesOrder as $key => $value) {
          $orderData = $this->wooCommerceOrder->wooCommerceGetOrder($value->id);
          foreach ($orderData->meta_data as $key => $value) {
            if($value->value == $mktplaceOrderId) return true;
          }
        }
        return false;
      }

      foreach ($salesOrder as $key => $value) {
        if($key < count($salesOrder)-$limit) $last_orders[] = $value;
      }
      foreach ($last_orders as $key => $value) {
        $orderData = $this->wooCommerceOrder->wooCommerceGetOrder($value->id);
        foreach ($orderData->meta_data as $key => $value) {
          if($value->value == $mktplaceOrderId) return true;
        }
      }
      return false;

    } catch(Exception $e) {
      $error = new error_handling("WCB2W: Erro ao buscar pedidos","Tentativa falha de buscar a lista de pedidos", "Erro<br>".$e->getMessage(), "Erro Pedido");
      $error->send_error_email();
      $error->execute();
      exit("Skyhub com problemas. Não foi possível retornar os pedidos");
    }
  }

  public function get_label($orderId) {
    $orderId = substr($orderId,strpos($orderId,'-')+1);

    $orderList = json_decode($this->b2wGetOrdersPlp()->response);
    foreach($orderList->orders as $key => $value) {
      if($orderId == $value->code) {
        $validate = true;
        break;
      } else $validate = false;
    }
    if($validate) {
      $validate = json_decode($this->b2wPostOrdersPlp([$orderId])->response);
      $httpCode = str_replace('HTTP/1.1 ','',$validate->response_status_lines[0]);
      $httpCode = (int) $httpCode;

      if($httpCode != 200) return false;

      $groupedPlps = json_decode($this->b2wGetGroupedPlp()->response);
      foreach ($groupedPlps->plp as $key => $value) {
        if($orderId == $value->orders[0]->code) $plpId = $value->id;
      }
      $plpPdf = $this->b2wGetOrderPlp($plpId);

      return $plpPdf;
    } else {
      $groupedPlps = json_decode($this->b2wGetGroupedPlp()->response);
      foreach ($groupedPlps->plp as $key => $value) {
        if($orderId == $value->orders[0]->code) $plpId = $value->id;
      }

      if(isset($plpId)) {
        $plpPdf = $this->b2wGetOrderPlp($plpId);
        return $plpPdf;
      }
    }

    return false;
  }
}
?>

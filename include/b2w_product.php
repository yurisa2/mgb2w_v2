<?php
class b2wwc_product extends products
{
  public function __construct()
  {
    parent::__construct();
    $this->product_id = '';
    $this->product_name = '';
    $this->product_price = '';
    $this->product_stock = '';
    $this->product_image = '';
    $this->product_weight = '';
    $this->product_description = '';
    $this->product_length = '';
    $this->product_height = '';
    $this->product_width = '';

    $this->wooCommerceProduct = new wooCommerceProduct;
  }

  public function product_update()
  {
    if($this->product_stock > 0) $product_status = "enabled";
    else $product_status = "disabled";

    if(TITLE) $product['name'] = PREFFIX_PROD.$this->product_name.SUFFIX_PROD;
    else echo '<h2>TITULO DESATIVADO</h2>';

    if(DESCRIPTION) $product['description'] = $this->product_description;
    else echo '<h2>DESCRIPTION DESATIVADO</h2>';

    if(!is_null($this->product_weight)) $product['weight'] = $this->product_weight;

    if(!is_null($this->product_height)) $product['height'] = $this->product_height;

    if(!is_null($this->product_length)) $product['length'] = $this->product_length;

    if(!is_null($this->product_width)) $product['width'] = $this->product_width;

    $additional_settings = $this->product_price * SETTINGS_PRICE_MULTIPLICATION + SETTINGS_PRICE_ADDITION;

    if(PRICE) $product['price'] = round($this->product_price / (1-COMMISSION) + $additional_settings,2);
    else echo '<h2>PRICE DESATIVADO</h2>';

    echo "Adicional ".$additional_settings.' + Comissão'.round($this->product_price / (1-COMMISSION),2).'<br>';
    echo "<br>Preço final ".$product['price'].'<br>';

    if(STOCK) $product['qty'] = $this->product_stock;
    else echo '<h2>STOCK DESATIVADO</h2>';

    $product['status'] = $product_status;

    $product['brand'] = BRAND;

    if(IMAGES) $product['images'] = $this->product_image;
    else echo '<h2>IMAGES DESATIVADO</h2>';

    $return = $this->update_product($this->product_sku,$product);

    $return = $this->verifyHttpStatus($return->response_status_lines);
    return $return;
  }

  private function verifyHttpStatus($status)
  {
    if(count($status) == 2) {
        if(is_bool(strpos($status[0],"10")) || is_bool(strpos($status[1],"20"))) {
          $error = new error_handling("WCB2W: Erro ao atualizar produto","Erro ao tentar atualizar produto<br>".serialize($status), "Produto: $this->product_sku<br>Titulo: $this->product_name", "Erro produto");
          $error->send_error_email();
          $error->execute();
          return "Erro ao atualizar produto $this->product_sku";
        } else $return = "Produto $this->product_sku <b>OK</b>";
    } else {
        if(is_bool(strpos($status[0],"20"))) {
          $error = new error_handling("WCB2W: Erro ao atualizar produto", "Erro ao tentar atualizar produto<br>".serialize($status), "Produto: $this->product_sku<br>Titulo: $this->product_name", "Erro produto");
          $error->send_error_email();
          $error->execute();
          return "Erro ao atualizar produto $this->product_sku";
        } else $return = "Produto $this->product_sku <b>OK</b>";
    }
    return $return;
  }
}
?>

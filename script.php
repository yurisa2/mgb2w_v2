<?php
ini_set("error_reporting",E_ALL);
ini_set('display_errors', 1);

require_once 'include/all_include.php';

echo "<pre>";

echo "<h1>PRODUTO</h1>";
$b2wWcProduct = new b2wwc_product;

$fluxProduct = new flux('b2w_product');
$fluxProduct->nfeFile = false;
$fluxProduct->pathListItem = true;
$fluxProduct->storeOrderList = false;
$fluxProduct->timeFile = true;
if(!$fluxProduct->setFiles()) {
  $listProductId = $b2wWcProduct->get_products_list();
  $fluxProduct->list_item = $listProductId;
  $fluxProduct->setFiles();
}
$fluxProduct->getFiles();
$fluxProduct->addCounter();
// $page = $fluxProduct->getPaginationProduct();
// $list_product_id = $b2wWcProduct->wooCommerceGetProductsIdSku($page);

$productSku = $fluxProduct->test_next_item();
// $productSku = "EP-51-40971";

if(!$productSku) echo "Todos os itens foram usados. Recomeçando a partir da proxima execução";
else {
  $productId = $b2wWcProduct->wooCommerceProduct->wooCommerceGetProductId($productSku);
  $productInfo = $b2wWcProduct->wooCommerceProduct->wooCommerceGetProduct($productId)->product;

  foreach ($productInfo->images as $key => $value) {
    $images[] = $value->src;
  }
  $b2wWcProduct->product_id = $productInfo->id;
  $b2wWcProduct->product_sku = $productInfo->sku;
  $b2wWcProduct->product_name = $productInfo->title;
  $b2wWcProduct->product_price = $productInfo->price;
  $b2wWcProduct->product_stock = $productInfo->stock_quantity;

  $b2wWcProduct->product_height = (float)$productInfo->dimensions->height/10;
  $b2wWcProduct->product_length = (float)$productInfo->dimensions->length/10;
  $b2wWcProduct->product_width = (float)$productInfo->dimensions->width/10;
  $b2wWcProduct->product_weight = (float)$productInfo->weight;

  $b2wWcProduct->product_image = $images;
  $b2wWcProduct->product_description = $productInfo->description;

  echo 'Preço no Wc '.$productInfo->price.'<br>';
  echo "Estoque no Wc ".$b2wWcProduct->product_stock.'<br>';
  echo "Altura no Wc ".$productInfo->dimensions->height.'(cm)<br>';
  echo "Comprimento no Wc ".$productInfo->dimensions->length.'(cm)<br>';
  echo "Largura no Wc ".$productInfo->dimensions->width.'(cm)<br>';

  echo $b2wWcProduct->product_update();

  $fluxProduct->add_item($productSku);
}


// ORDER
// if($_SERVER['SERVER_NAME'] == 'localhost') echo '<br>SCRIPT RODANDO EM LOCALHOST. ABORTAR BLOCO DE PEDIDO - ETIQUETA - NFE. <BR>RODAR SCRIPT EM easypath.com.br/conectores/mgml/script.php</b>';
// else {
echo "<h1>PEDIDO</h1>";
$b2w_order = new b2w_order;
$fluxOrder = new flux('b2w_order');
$fluxOrder->nfeFile = true;
$fluxOrder->pathListItem = true;
$fluxOrder->storeOrderList = true;
if(!$fluxOrder->setFiles()) {
  // get b2w order list only if status equals APPROVED
  $listOrderId = $b2w_order->get_order_list();
  $fluxOrder->list_item = $listOrderId;
  $fluxOrder->setFiles();
}
$fluxOrder->getFiles();
if(!$fluxOrder->list_item) echo "Não há novos pedidos<br>";
else {
  /* get the next order to be insert */
  $order_id = $fluxOrder->next_item();
  /* if return false, not be new orders */
  if(!$order_id) echo "Sem novos pedidos<br>";
  else {
    /* if the next order not exists in array returned by getOrderStoreList() and
    doesn't have any order in wc with this order id create a new order */
    $last_order_b2w = $b2w_order->get_last_b2wOrder($order_id,10);
    if(is_bool(array_search($order_id,$fluxOrder->getOrderStoreList())) && !$last_order_b2w) {
      /* get content in helper_label */
      $timerHelper = $fluxProduct->getTimer();
      /* if null add order id in b2w_order_nfe and create, if not exists,
      the file helper_label with order id and the time (in timestamp) */
      if (!$timerHelper['waitTimeOrder']) {
        $fluxOrder->addOrderNfe($order_id);
        $fluxOrder->addLabelHelper($order_id);
        $fluxOrder->addTimer('waitTimeOrder',time());
        echo "Sem etiquetas para baixar<br>";
      } else {
        echo "Pedido $order_id <br>";
        /* if the time of helper_label plus 300(s) is less than time now create the order */
        if($timerHelper['waitTimeOrder']+300 < time()) {

          $labelHelper = (array)$fluxOrder->getLabelHelper();

          $label = $b2w_order->get_label($labelHelper['order']);
          if(!$fluxOrder->createLabelOrder($labelHelper['order'],$label)) {
            $error = new error_handling("WCB2W: Erro ao copiar etiqueta", "Não foi possível copiar a etiqueta para /conectores/pedidos/", "Order: ".$labelHelper['order'], "Erro etiqueta");
            $error->send_error_email();
            $error->execute();
            echo "Erro inserir o pedido $order_id no WooCommerce. Retorno fora do esperado";
            echo "<b>$wcOrder</b>";
          } else {
            $order_data = json_decode($b2w_order->get_order_information($labelHelper['order'])->response);
            $email = new email($order_data);
            $email->sendOrderLabel();
            $fluxOrder->removeOrderNfe();
            echo "etiqueta criada";
            exit;
            $wcOrder = $b2w_order->orderCreate($order_id);
            /* if success, add the order id to list of orders added in wooCommerce
            if failure, send a email*/
            if($wcOrder) {
              $json_order = $fluxOrder->addOrderStore($order_id);
            } else {
              $error = new error_handling("WCB2W: Erro ao criar pedido no WooCommerce", "Retorno diferente do esperado. Esperando id (int)", "$wcOrder", "Erro json");
              $error->send_error_email();
              $error->execute();
              echo "Erro inserir o pedido $order_id no WooCommerce. Retorno fora do esperado";
              echo "<b>$wcOrder</b>";
            }
            var_dump($fluxOrder->add_item($order_id));
            var_dump($fluxOrder->addTimer('waitTimeOrder',false));
          }
        } else {
          echo "Pedido deve esperar 5 min (300s) para ser processado e criado. Faltam: ";
          echo ((int)$timerHelper['waitTimeOrder']+300) - time();
        }
      }
    } else echo "Já inserido no WooCommerce<br>";
  }
}

// INVOICES
//if was been 24 hours of last time was checked
// $list_order_id = $b2w_order->get_order_list();
if(!$fluxOrder->list_item) exit("Não há pedidos sem a nota fiscal eletrônica<br>");

$fluxOrder->pathFiles = $fluxOrder->lastNfeFile;
$fluxOrder->list_item = json_decode(file_get_contents($fluxOrder->nfeFile));
$fluxOrder->setFiles();
$fluxOrder->getFiles();
$order_id = $fluxOrder->next_item();
if(!$order_id) exit("Não há novos pedidos");
echo "<h3>$order_id</h3>";
$order_data = json_decode($b2w_order->get_order_information($order_id)->response);

if((empty($order_data->invoices)) && ($order_data->status->type == "APPROVED")) {
  echo '<br>Contador: '.$fluxOrder->counter.'<br>';
  echo "<br>Verificar NFe a cada ".VERIFY_NFE." seg<br>";

  if($timerHelper['nfe'] + VERIFY_NFE < time()) {
    $log = new log("Pedido B2W sem NFE", "Pedido $order_id sem chave NFE", " Favor visitar o link: ".NFE_URL, "pedido sem NFE");
    $log->log_email = true;
    $log->mensagem_email = "Pedido B2W sem NFE";
    $log->log_email = true;
    $log->email_nfe = true;
    $log->dir_file = "log/log.json";
    $log->log_files = true;
    $log->send_log_email();
    $log->execute();
    $fluxProduct->addTimer('nfe',time());
  } else {
    $tnfe = ($timerHelper['nfe'] + VERIFY_NFE) - time();
    echo '<br>Próximo email de notificação de NFe em '.$tnfe.' seg.';
  }
} else {
  // $fluxOrder->add_item($order_id);
  echo '<br>Não há pedidos sem chave nfe e com status Aprovado ou Enviado';
}
// }
?>
